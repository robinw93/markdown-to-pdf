---
author: John Doe
title: SDD
subtitle: Software Design Document
date: \today
titlepage: true
titlepage-logo: logo.png
header-left: logo.png
header-right: \title{$title$}
footer-left: "This is a disclaimer to not copy and distribute, unless written permission has been provided."
# graphics: true
toc: true
# toc-own-page: true
lof: true
lot: true
signers:
    - name: John Doe
      role: Lead Software Engineer
    - name: Hendrik
      role: Oelewapper
---

# Example of code snippets
## C example
A C code snippet can be found in \ref{c-code}

```{.C caption="c code snippet" label="c-code"}
#include <stdio.h>

int main() 
{
 int total = 100;
 int i = 10;
 printf("The initial value of total and i is : %d, %d", total, i);
 total = ++i;
 printf("\nAfter applying ++i value of total and i is : %d, %d", total, i);
 total = 100;
 i = 10;
 printf("\n\nThe initial value of total and i is : %d, %d", total, i);
 total = i++;
 printf("\nAfter applying i++ value of total and i is : %d, %d", total, i);
 return 0;
}
```

## A Python Example
But also in Python

```Python
def func():
    echo("Hello world")
```

# Example of images
this is an example of an image that can be seen in \ref{my-image}.

![This is the caption \label{my-image}](logo.png)

# Additional features

Please look at the table at \ref{the-table}

|   header	|   asdf	|
|:-:	    |:-:	    |
|   1	    |   	2   |
|   1	    |   2	    |
Table: Demonstration of table \label{the-table}
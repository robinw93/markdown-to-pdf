#!/bin/bash
set -eou pipefail

DATA_DIR=data
INPUT_DIR=input
INTERMEDIATE_DIR=intermediate
OUTPUT_DIR=output

[ ! -d ${DATA_DIR}/${INTERMEDIATE_DIR} ] && mkdir -p ${DATA_DIR}/${INTERMEDIATE_DIR}
[ ! -d ${DATA_DIR}/${OUTPUT_DIR} ] && mkdir -p ${DATA_DIR}/${OUTPUT_DIR}
[ ! -d ${DATA_DIR}/${INPUT_DIR} ] && mkdir -p ${DATA_DIR}/${INPUT_DIR}


docker compose exec pandoc pandoc -s --data-dir . --template=template.latex -V version="1.0.0" --output=${INTERMEDIATE_DIR}/output.tex ${INPUT_DIR}/input.md --listings
docker compose exec pandoc pdflatex -output-directory=${OUTPUT_DIR} ${INTERMEDIATE_DIR}/output.tex 

